package com.c0d3in3.lecture6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    var number = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

    }

//    override fun onClick(v: View?) { // to make this work firstly we have to implement View.OnClickListener as parent of this class
//        v as Button //casting view object as Button
//        if (v != null) { // checking if view is not null
//            when(v.id){
//                R.id.minusButton -> {
//                    modifyNumber(-1) // calling method to decrease number
//                }
//
//                R.id.plusButton -> {
//                    modifyNumber(1) // calling method to increase number
//                }
//            }
//        }
//    }

    private fun init() {

//        minusButton.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(v: View?) {
//                modifyNumber(-1)
//
//            }
//        })
//
//        plusButton.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(v: View?) {
//                modifyNumber(1)
//
//            }
//        })

        minusButton.setOnClickListener {
            modifyNumber(-1) // calling method to decrease number
        }
//
//        plusButton.setOnClickListener {
//            modifyNumber(1) // calling method to increase number
//        }

        plusButton.setOnLongClickListener {
            modifyNumber(1)
        }
    }

    private fun modifyNumber(value: Int): Boolean {
        if (number + value < 0 || number + value > 10) { // checking if number is in range [1;10]
            Toast.makeText(this, "რიცხვი ვერ იქნება 0-ზე ნაკლები და 10-ზე მეტი!", Toast.LENGTH_LONG)
                .show()
            return true
        }
        number += value
        numberTextView.text = number.toString()
        return true
    }

}
